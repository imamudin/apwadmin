package com.legendjava.apwadmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.legendjava.apwadmin.app.MyAppController;
import com.legendjava.apwadmin.config.GlobalConfig;
import com.legendjava.apwadmin.mysp.ObscuredSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;
public class LoginActivity extends AppCompatActivity {
    Button btn_login;
    EditText et_user_name, et_password;
    String s_user_name, s_password;

    ObscuredSharedPreferences pref;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cek apakah hp konek internet
                ConnectivityManager cm =
                        (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    //cek email dan passwod tidak boleh kosong
                    s_user_name     = et_user_name.getText().toString().trim();
                    s_password      = et_password.getText().toString().trim();

                    if(s_user_name.length() != 0 && s_password.length() != 0){
                        login();
                    }else{
                        notifikasi_dialog("",GlobalConfig.notif_form_tidak_kosong);
                    }
                }else{
                    notifikasi_dialog("",GlobalConfig.notif_butuh_koneksi);
                }
            }
        });
    }
    @Override
    protected void onDestroy() {
        try {
            if (loading != null && loading.isShowing()) {
                loading.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
    private void init(){
        btn_login       = (Button)findViewById(R.id.btn_login);
        et_user_name    = (EditText)findViewById(R.id.et_user_name);
        et_password     = (EditText)findViewById(R.id.et_password);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        if(pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openMainActivity();
            Log.d("login", "buka main");
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void login(){
        Log.d(GlobalConfig.TAG, "make progress dialog");
        loading = ProgressDialog.show(LoginActivity.this, "Login...", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGIN;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_NAME, s_user_name);
            jsonBody.put(GlobalConfig.USER_PASWORD, s_password);

            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject data = response.getJSONObject("data");
                            pref.edit().putBoolean(GlobalConfig.IS_LOGIN, true).commit();
                            pref.edit().putString(GlobalConfig.USER_ID, data.getString(GlobalConfig.USER_ID)).commit();
                            pref.edit().putString(GlobalConfig.USER_TOKEN, data.getString(GlobalConfig.USER_TOKEN)).commit();

                            openMainActivity();
                        }else{
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};
            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    //Somewhere that has access to a context
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    private void openMainActivity(){
        Intent dashboard = new Intent(LoginActivity.this, Dashboard.class);
        startActivity(dashboard);
        LoginActivity.this.finish();
    }
}

