package com.legendjava.apwadmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.legendjava.apwadmin.app.MyAppController;
import com.legendjava.apwadmin.config.GlobalConfig;
import com.legendjava.apwadmin.listAdapter.ListAdapterToko;
import com.legendjava.apwadmin.model.TokoModel;
import com.legendjava.apwadmin.mysp.ObscuredSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 12/03/17.
 */
public class PilihToko extends AppCompatActivity
{
    String search;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;

    //variabel untuk list view
    private List<TokoModel> PilihTokoList = new ArrayList<TokoModel>();
    private ListView listView;
    private ListAdapterToko adapter;

    private SwipeRefreshLayout swipeContainer;
    JsonObjectRequest request;

    int offSet=0;

    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view

    //untuk handler AsyncTask
    protected static final int MSG_REGISTER_WEB_SERVER_SUCCESS = 103;
    protected static final int MSG_REGISTER_WEB_SERVER_FAILURE = 104;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pilih_toko);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Pilih Toko");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        Intent old  = getIntent();
        search      = old.getStringExtra("search");

        //Log.d(GlobalConfig.TAG, search);

        //untuk list view
        listView = (ListView)findViewById(R.id.custom_list);
        PilihTokoList.clear();
        adapter = new ListAdapterToko(PilihToko.this, PilihTokoList);
        listView.setAdapter(adapter);

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                //fetchTimelineAsync(0);
                PilihTokoList.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
                Toast.makeText(PilihToko.this,"refresh",Toast.LENGTH_LONG).show();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = listView.getItemAtPosition(position);
                TokoModel tokoModel = (TokoModel) o;

                openToko(tokoModel.KODE);
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                PilihTokoList.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        handler = new Handler();

                        runnable = new Runnable() {
                            public void run() {
                                callNews(offSet);

                            }
                        };
                        //untuk menerlambatkan 1 detik
                        handler.postDelayed(runnable, 1000);
                    }else{
                        //Toast.makeText(PilihToko.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }
    private void callNews(int page){
        swipeContainer.setRefreshing(true);
        // Creating volley request obj
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+ GlobalConfig.URL_TOKOS;

        //Log.d(GlobalConfig.TAG, url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
            jsonBody.put("search", search);


            jsonBody.put(GlobalConfig.UP_START, page);
            jsonBody.put(GlobalConfig.UP_LIMIT, GlobalConfig.MAX_ROW_PER_REQUEST);

            //Log.d(GlobalConfig.TAG,"search : "+search);
            //Log.d(GlobalConfig.TAG,""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Log.d(GlobalConfig.TAG, ""+response.toString());
                    try {
                        int status  = response.getInt("status");
                        String pesan= response.getString("message");
                        Log.d(GlobalConfig.TAG, ""+status);
                        if(status==1){
                            JSONObject respon_data  = response.getJSONObject("data");
                            int total               = respon_data.getInt("total");

                            if(total<GlobalConfig.MAX_ROW_PER_REQUEST || total==0) {
                                disableSwipeDown = true;
                            }
                            offSet += total;
                            JSONArray datas = respon_data.getJSONArray("toko");

                            if(datas.length()>0) {
                                for (int i = 0; i < datas.length(); i++) {
                                    JSONObject data = datas.getJSONObject(i);

                                    TokoModel tokoModel = new TokoModel(data.getString(GlobalConfig.GT_KODE),data.getString(GlobalConfig.GT_NAMA),
                                            data.getString(GlobalConfig.GT_ALAMAT));

                                    // adding news to news array
                                    PilihTokoList.add(tokoModel);
                                    swipeContainer.setRefreshing(false);

                                    adapter.notifyDataSetChanged();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),"Toko tidak ditemukan.", Toast.LENGTH_SHORT).show();
                                swipeContainer.setRefreshing(false);
                            }
                        }else{
                            swipeContainer.setRefreshing(false);
                            Toast.makeText(getApplicationContext(),pesan , Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    //Log.d("respons", error.getMessage());
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            //Adding request to the queue
            // Adding request to request queue
            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void openToko(String id_toko){
        Log.d(GlobalConfig.TAG, "scan toko");
        loading = ProgressDialog.show(PilihToko.this, "Memproses data", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_SCANTOKO;

        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
            jsonBody.put(GlobalConfig.ID_TOKO, id_toko);

            //Log.d(GlobalConfig.TAG, ""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    //Log.d(GlobalConfig.TAG, response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject data = response.getJSONObject("data");

                            JSONObject toko = data.getJSONObject("toko");
                            pref.edit().putBoolean(GlobalConfig.IS_TOKO, true).commit();
                            pref.edit().putString(GlobalConfig.ID_TOKO, toko.getString(GlobalConfig.ID_TOKO)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_NAMA, toko.getString(GlobalConfig.GTOKO_NAMA)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_ALAMAT, toko.getString(GlobalConfig.GTOKO_ALAMAT)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_PIMPINAN, toko.getString(GlobalConfig.GTOKO_PIMPINAN)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_NOTELP, toko.getString(GlobalConfig.GTOKO_NOTELP)).commit();
                            pref.edit().putString(GlobalConfig.GREAL_PLAFON, toko.getString(GlobalConfig.GREAL_PLAFON)).commit();
                            pref.edit().putString(GlobalConfig.GKDSALES, toko.getString(GlobalConfig.GKDSALES)).commit();
                            pref.edit().putString(GlobalConfig.GJATUHTEMPOBAYAR, toko.getString(GlobalConfig.GJATUHTEMPOBAYAR)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_LAT, toko.getString(GlobalConfig.GTOKO_LAT)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_LONG, toko.getString(GlobalConfig.GTOKO_LONG)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_NAMA_SALES, toko.getString(GlobalConfig.GTOKO_NAMA_SALES)).commit();

                            Intent i_toko = new Intent(PilihToko.this, Toko.class);
                            startActivity(i_toko);
                        }else{
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_WEB_SERVER_SUCCESS:
                    Toast.makeText(getApplicationContext(),
                            "Tidak mendapatkan respon.", Toast.LENGTH_LONG).show();
                    break;
                case MSG_REGISTER_WEB_SERVER_FAILURE:
                    Toast.makeText(getApplicationContext(),
                            "Tidak dapat mengakses server.",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 1, 1, "Semua").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(PilihToko.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
}
